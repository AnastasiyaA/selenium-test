package com.example.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Iterator;
import java.util.Set;

public class NavigationFunctions {
    protected Manager manager;
    protected WebDriver driver;
    private String url;
    private String requestText;

    public NavigationFunctions(Manager manager, String url, String requestText) {
        this.manager = manager;
        this.driver = manager.driver;
        this.url = url;
        this.requestText = requestText;
    }

    public void openBistrodengi() { // todo
        driver.get(url);
        driver.findElement(By.id("text")).clear();
        driver.findElement(By.id("text")).sendKeys(requestText);
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        driver.findElement(By.linkText("«Быстроденьги» — срочные займы - Казань")).click();
//        System.out.println(driver.getCurrentUrl());

        // не работало. открывал новую вкладку "быстроденьги", но title выводил у яндекса. добавив этот код, все заработало
        String parentWindowHandler = driver.getWindowHandle();
        String subWindowHandler = null;

        Set<String> handles = driver.getWindowHandles();
        Iterator<String> iterator = handles.iterator();
        while (iterator.hasNext()) {
            subWindowHandler = iterator.next();
            driver.switchTo().window(subWindowHandler);
//            System.out.println(subWindowHandler);
        }

        driver.switchTo().window(subWindowHandler);

    }
}