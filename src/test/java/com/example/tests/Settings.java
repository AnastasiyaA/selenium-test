package com.example.tests;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;

public class Settings {
    public static String settingsFile = "settings.xml";
    private static String url;
    private static String requestText;

    Settings() throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
    }

    public static String getParameter(String parameter) throws XPathExpressionException, IOException, SAXException {
        String expression;
        if ( parameter.equals("url") ) {
            if (url == null){
                expression = "Settings/Url";
            } else {
                return url;
            }
        } else if ( parameter.equals("requestText") ) {
            if (requestText == null){
                expression = "Settings/RequestText";
            } else {
                return requestText;
            }
        } else {
            expression = "Settings/Error";
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        Document doc = builder.parse( settingsFile );

        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr = xpath.compile(expression);
        NodeList nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

        if (nl.getLength()>0){
            return nl.item(0).getTextContent();
        } else {
            return "";
        }
    }
}
