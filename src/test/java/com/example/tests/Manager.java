package com.example.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.xml.sax.SAXException;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Manager {
    protected WebDriver driver;
    protected String url;
    protected String requestText;
    private NavigationFunctions navigator;
    private static ThreadLocal<Manager> man;

    private Manager() {
        System.setProperty("webdriver.gecko.driver","C:\\Users\\Анастасия\\Downloads\\geckodriver-v0.18.0-win64\\geckodriver.exe");

        try {
            url = Settings.getParameter("url");
            requestText = Settings.getParameter("requestText");
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        navigator = new NavigationFunctions(this, url, requestText);
    }

    public static Manager getInstance(){
        if (man == null){
            man = new ThreadLocal<Manager>();
            man.set(new Manager());
        }
        return man.get();
    }

    public boolean checkIsValid() throws Exception{
        System.out.println(driver.getTitle());
        return driver.findElement(By.id("main_calc_top")).isDisplayed();
    }

    public NavigationFunctions getNavigator() {
        return navigator;
    }

    public void stop(){
        driver.quit();
    }
}