package com.example.tests;

import org.junit.AfterClass;
import org.junit.Before;

public class TestBase {
    public static Manager man;

    @Before
    public void setUp() throws Exception{
        man = Manager.getInstance();
    }

    @AfterClass
    public static void tearDown() throws Exception{
        man.stop();
    }
}

